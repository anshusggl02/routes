import React from "react";
import { NavLink } from "react-router-dom";

import "./MainHeader.css";

const MainHeader = () => {
  return (
    <div>
      <header>
        <nav>
          <ul>
            <li className="welcome">
              <NavLink to="/welcome"> Welcome </NavLink>
            </li>
            <li className="product">
              <NavLink to="/product"> Product </NavLink>
            </li>
          </ul>
        </nav>
      </header>
    </div>
  );
};
export default MainHeader;
