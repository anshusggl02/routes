import React from "react";
import { Link } from "react-router-dom";

import "./product.css";

const Product = () => {
  return (
    <div>
      <h2> Product </h2>
      <ul className="product-list">
        <li>
          <Link to="Product1">Product 1 </Link>
        </li>
        <li>
          <Link to="Product2"> Product 2 </Link>
        </li>
        <li>
          <Link to="Product3">Product 3 </Link>
          <li>
            <Link to="Product4"> Product 4</Link>
          </li>
        </li>
      </ul>
    </div>
  );
};

export default Product;
