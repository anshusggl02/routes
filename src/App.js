import React from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";

import Welcome from "./pages/Welcome";
import Products from "./pages/Product";
import MainHeader from "./components/MainHeader";
import ProductDetail from "./pages/ProductDetail";
import NewUser from "./pages/NewUser";

function App() {
  return (
    <div>
      <Router>
        <MainHeader />

        <Routes>
          <Route exact path="/welcome" element={<Welcome />} />
          <Route exact path="/welcome/NewUser" element={<NewUser />} />
          <Route path="/product" element={<Products />} />
          <Route path="/Product/:productId" element={<ProductDetail />} />
        </Routes>
      </Router>
    </div>
  );
}

export default App;
